<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @ORM\Entity()
 */
class ContractPart
{
    const TYPE_NUTRITION = 'nutrition';
    const TYPE_MOBILITY = 'mobility';
    const TYPE_PERSONAL_CONSUMPTION = 'personal_consumption';
    const TYPE_ENERGY = 'energy';
    const TYPE_COMMUNICATION_KNOW_HOW = 'communication_know_how';
    const TYPE_CUSTOM = 'custom';

    public static $TYPES = [
        self::TYPE_NUTRITION,
        self::TYPE_MOBILITY,
        self::TYPE_PERSONAL_CONSUMPTION,
        self::TYPE_ENERGY,
        self::TYPE_COMMUNICATION_KNOW_HOW,
        self::TYPE_CUSTOM,
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
