<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @ORM\Entity()
 */
class Contract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $fileName;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $active = false;
    /**
     * @var SignedContract[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="SignedContract", mappedBy="contract")
     */
    private $signedContracts;
    /**
     * @var ContractPart[];
     * @ORM\ManyToMany(targetEntity="ContractPart")
     */
    private $selectableContractParts;


    public function __construct()
    {
        $this->createdDate = new \DateTime('now');
        $this->signedContracts = new ArrayCollection();
        $this->selectableContractParts = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onCreate(): void
    {
        $this->createdDate = new \DateTime('now');
    }

    /**
     * @return SignedContract[]|ArrayCollection
     */
    public function getSignedContracts()
    {
        return $this->signedContracts;
    }

    /**
     * @return ContractPart[]|PersistentCollection
     */
    public function getSelectableContractParts(): PersistentCollection
    {
        return $this->selectableContractParts;
    }

    public function addSelectableContractPart(ContractPart $contractPart): void
    {
        $this->selectableContractParts->add($contractPart);
    }

    public function removeSelectableContractPart(ContractPart $contractPart): void
    {
        $this->selectableContractParts->removeElement($contractPart);
    }
}
