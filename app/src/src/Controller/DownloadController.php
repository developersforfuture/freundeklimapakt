<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class DownloadController extends AbstractController
{
    private static $mapping = [
        'freundeklimapakt.pdf' => 'contract.pdf',
        'FactSheet.pdf' => 'facts.pdf'
    ];

    public function index(string $file): Response
    {
        $response = new BinaryFileResponse(__DIR__.'/../Resources/documents/'.self::$mapping[$file]);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file);

        return $response;
    }
}
