<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Entity\ContractPart;
use App\Entity\SignedContract;
use App\Entity\Subscription;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Swift_RfcComplianceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ContractController extends AbstractController
{
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger
    ): Response {
        $latestContract = $entityManager->getRepository(Contract::class)->findOneBy(['active' => true]);
        if (!$latestContract instanceof Contract) {
            $logger->critical('No contract with aktive = true found.');
            throw  new RouteNotFoundException('There is now actual contract available atm.');
        }
        $contractPartsByType = [];
        foreach ($latestContract->getSelectableContractParts() as $part) {
            if (!isset($contractPartsByType[$part->getType()])) {
                $contractPartsByType[$part->getType()] = [];
            }
            $contractPartsByType[$part->getType()][] = $part;
        }
        $form = $this->createSubscriptionForm($contractPartsByType);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = $this->generateUuid();

            $logger->info('Verification token created: ' . $token);
            /** @var SignedContract $signedContract */
            $signedContract = $form->getData();
            $existingSubscription = $entityManager->getRepository(SignedContract::class)
                ->findOneBy(['email' => $signedContract->getEmail()]);
            if ($existingSubscription instanceof SignedContract) {
                $logger->warning('Second try with same email address');

                return $this->render(
                    'pages/contract/index.html.twig',
                    [
                        'form' => $form->createView(),
                        'contractPartsByType' => $contractPartsByType,
                        'error' => 'Email existiert bereits: ' . $signedContract->getEmail(),
                    ]
                );
            }
            $activeContract = $entityManager->getRepository(Contract::class)->findOneBy(['active' => 1]);
            foreach ($contractPartsByType as $type => $parts) {
                if ($form->has('selectedContractParts-' . $type)) {
                    $partFormData = $form->get('selectedContractParts-' . $type)->getData();
                    if (is_array($partFormData)) {
                        foreach ($partFormData as $part) {
                            if ($part instanceof ContractPart) {
                                $signedContract->addSelectedContractPart($part);
                            }
                        }
                    }
                }
            }

            $signedContract->setIsVerified(false);
            $signedContract->setContract($activeContract);
            $signedContract->setVerificationToken($token);
            $signedContract->setCreatedDate(new \DateTime('now'));
            try {
                $result = $this->sendVerificationMail($mailer, $signedContract, $token);
            } catch (\Swift_TransportException $e) {
                $logger->error(
                    'Problems to send mail',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $signedContract->getEmail(),
                    ]
                );

                return $this->render(
                    'pages/contract/index.html.twig',
                    [
                        'form' => $form->createView(),
                        'contractPartsByType' => $contractPartsByType,
                        'error' => 'Problems to send Mail. Please try again.',
                    ]
                );
            } catch (Swift_RfcComplianceException $e) {
                $logger->warning(
                    'Non valid email address. could not be sendt.',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $signedContract->getEmail(),
                    ]
                );
            }
            $entityManager->persist($signedContract);
            $entityManager->flush();

            $logger->debug('Signed contract persisted and needs no baby doll');
            return $this->redirectToRoute('successful_contract_request');
        }

        return $this->render(
            'pages/contract/index.html.twig',
            [
                'form' => $form->createView(),
                'contractPartsByType' => $contractPartsByType,
            ]
        );
    }

    public function verification(string $token, EntityManagerInterface $entityManager, LoggerInterface $logger): Response
    {
        /** @var SignedContract $existingSubscription */
        $existingSubscription = $entityManager->getRepository(SignedContract::class)
            ->findOneBy(['verificationToken' => $token, 'isVerified' => false]);

        if (!$existingSubscription instanceof SignedContract || $existingSubscription->isVerified()) {
            $logger->info('No contract to sign found to verify or a second request to verify. Tryed for token: ' . $token);

            return new Response('', Response::HTTP_NOT_FOUND);
        }

        $existingSubscription->setIsVerified(true);
        $existingSubscription->setVerificationDate(new \DateTime('now'));
        $entityManager->persist($existingSubscription);
        $entityManager->flush();

        $logger->info('Successful verification');

        return $this->render('pages/contract/verification.html.twig', []);
    }

    /**
     * @param ContractPart[][] $contractPartsByType
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createSubscriptionForm(array $contractPartsByType)
    {
        $subscription = new SignedContract();
        $formBuilder = $this->createFormBuilder($subscription)
            ->add('name', TextType::class, ['label' => 'form.field.label.name', 'required' => true])
            ->add('email', TextType::class, ['label' => 'form.field.label.email', 'required' => true]);
        $counter = 0;
        foreach ($contractPartsByType as $type => $parts) {
            if (0 == count($parts)) {
                continue;
            }
            $formBuilder
                ->add(
                    'selectedContractParts-' . $type,
                    ChoiceType::class,
                    [
                        'mapped' => false,
                        'empty_data' => [],
                        'choices' => $parts,
                        'label' => 'form.field.legend.' . $type,
                        'choice_label' => false,
                        'multiple' => true,
                        'expanded' => true,
                    ]
                );
        }
        $formBuilder
            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'label' => false,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ],
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'form.field.label.submit']);
        $form = $formBuilder->getForm();

        return $form;
    }

    private function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    private function sendVerificationMail(\Swift_Mailer $mailer, SignedContract $signedContract, string $token): int
    {
        $message = (new \Swift_Message('Verifikation FreundeKlimaPakt'))
            ->setFrom('freundeklimapakt@developersforfuture.org')
            ->setTo($signedContract->getEmail())
            ->setBody(
                $this->renderView(
                    'pages/contract/email_verification.html.twig',
                    [
                        'subscription' => $signedContract,
                        'token' => $token,
                    ]
                ),
                'text/html'
            );

        return $mailer->send($message);
    }
}
