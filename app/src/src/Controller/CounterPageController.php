<?php

namespace App\Controller;

use App\Entity\SignedContract;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CounterPageController extends AbstractController
{
    public function index(EntityManagerInterface $entityManager)
    {
        $verifiedSignedContracts = $entityManager->getRepository(SignedContract::class)->findBy(['isVerified' => true]);
        $types = [];
        foreach ($verifiedSignedContracts as $contract) {
            foreach ($contract->getSelectedContractParts() as $part) {
                if (!isset($types[$part->getType()])) {
                    $types[$part->getType()] = [];
                }
                if (!isset($types[$part->getType()][$part->getId()])) {
                    $types[$part->getType()][$part->getId()] = ['title' => $part->getTitle(), 'counter' => 0];
                }
                $types[$part->getType()][$part->getId()]['counter']++;
            }
        }

        return $this->render('pages/counter/index.html.twig', ['counter' => count($verifiedSignedContracts), 'types' => $types]);
    }
}
