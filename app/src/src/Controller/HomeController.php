<?php

namespace App\Controller;

use App\Entity\SignedContract;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class HomeController extends AbstractController
{
    public function index(EntityManagerInterface $entityManager)
    {
        $verifiedSignedContracts = $entityManager->getRepository(SignedContract::class)->findBy(['isVerified' => true]);
        return $this->render('pages/home/index.html.twig', ['counter' => count($verifiedSignedContracts)]);
    }
}
