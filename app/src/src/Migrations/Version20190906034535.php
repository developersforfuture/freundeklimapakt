<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Contract;
use App\Entity\ContractPart;
use App\Entity\SignedContract;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\AbortMigration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Parser;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190906034535 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function getDescription() : string
    {
        return 'Basic data import';
    }

    public function up(Schema $schema) : void
    {
        /** @var EntityManagerInterface $manager */
        $manager = $this->container->get('doctrine.orm.entity_manager');

        $contract = new Contract();
        $contract->setTitle('First Contract');
        $contract->setFileName('2019/dummy_contract.pdf');
        $contract->setActive(true);
        $manager->persist($contract);
        try {
            $this->loadContractParts($manager, $contract);
        } catch (\Exception $e) {
            throw new AbortMigration($e->getMessage(), $e->getCode(), $e);
        }

        $manager->flush();

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }


    private function loadContractParts(ObjectManager $manager, Contract $contract): void
    {
        $yaml = new Parser();
        $data = $yaml->parse(file_get_contents(__DIR__ . '/../Resources/data/2019/contract_parts.yaml'));
        $parts = [];
        foreach ($data['contract_parts'] as $contractPartData) {
            $contractPart = new ContractPart();
            $contractPart->setTitle($contractPartData['title']);
            if (!in_array($contractPartData['type'], ContractPart::$TYPES)) {
                throw  new \Exception('Unknown type found: '.$contractPartData['type']);
            }
            $contractPart->setType($contractPartData['type']);
            if (isset($contractPartData['description'])) {
                $contractPart->setDescription($contractPartData['description']);
            }
            $manager->persist($contractPart);
            $contract->addSelectableContractPart($contractPart);
        }
    }

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
