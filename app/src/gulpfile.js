const gulp              = require('gulp');
const jsminify          = require('gulp-terser');
const include           = require('gulp-include');
const { watch }         = require('gulp');

const paths = {
    scripts: {
        src: 'assets/js/src/*.js',
        dest: 'public/build/'
    },
};

function BuildJs() {
    return gulp.src(paths.scripts.src)
        .pipe(include())
        .pipe(jsminify())
        .pipe(gulp.dest(paths.scripts.dest));
}
function DevJs() {
    return gulp.src(paths.scripts.src)
        .pipe(include())
        .pipe(gulp.dest(paths.scripts.dest));
}

exports.watch = function() {
    watch(paths.scripts.src, BuildJs);
};

exports.BuildJs           = BuildJs;
exports.DevJs             = DevJs;