.PHONY:  kubernetes/app.production.yaml, Dockerfile, unit_test, release, dobi.yaml, push_tag
unit_test:
	@echo "+++ Unit tests +++"

override projectRootDir = ./
override projectVersion = $(VERSION_TAG)
override gitOriginUrl = $(shell git config --get remote.origin.url)
override projectName=freundeklimapakt
override projectRegistry=$(REGISTRY)
override projectPath=$(REPOSITORY_PATH)
override baseContainerPath=registry.gitlab.com/froscon/php-track-web
override baseContainerVersion=1.1.0
override mailUrl=$(MAILER_URL)
override releaseImage = $(REGISTRY)/$(REPOSITORY_PATH)/app:$(projectVersion)

override containerBasePath=$(REGISTRY)/$(REPOSITORY_PATH)/app-$(RUNTIME)
override dobiDeps = kubernetes/app.production.yaml kubernetes/app.development.yaml dobi.yaml Dockerfile docker-compose.dev.yaml docker_login
dobiTargets = shell build push autoclean

# helper macros
override getImage = $(firstword $(subst :, ,$1))
override getImageTag = $(or $(word 2,$(subst :, ,$1)),$(value 2))
override printRow = @printf "%+22s = %-s\n" $1 $2

override M4_OPTS = \
	--define "m4ProjectName=$(projectName)" \
	--define "m4ProjectVersion=$(projectVersion)" \
	--define "m4GitOriginUrl=$(gitOriginUrl)" \
	--define "m4ReleaseImage=$(call getImage, $(releaseImage))" \
	--define "m4ReleaseImageTag=$(call getImageTag, $(releaseImage),latest)" \
	--define "m4ContainerBasePath=$(containerBasePath)" \
	--define "m4BaseContainerPath=$(baseContainerPath)" \
	--define "m4MailerUrl=$(mailUrl)" \
	--define "m4baseContainerVersion=$(baseContainerVersion)" \
	--define "m4Runtime=$(RUNTIME)"

kubernetes/app.production.yaml: kubernetes/app.production.m4.yaml Makefile
	@echo "\n + + + Build Kubernetes app prod yml + + + "
	@m4 $(M4_OPTS) --define m4Runtime=production kubernetes/app.production.m4.yaml > kubernetes/app.production.yaml

kubernetes/app.development.yaml: kubernetes/app.development.m4.yaml Makefile
	@echo "\n + + + Build Kubernetes app dev yml + + + "
	@m4 $(M4_OPTS) --define m4Runtime=development  kubernetes/app.development.m4.yaml > kubernetes/app.development.yaml

Dockerfile: Dockerfile.m4
	@echo "\n + + + Build Dockerfile + + + "
	@m4 $(M4_OPTS) Dockerfile.m4 > Dockerfile

dobi.yaml: dobi.yaml.m4 Makefile
	@echo "\n + + + Build dobi.yaml + + + "
	@export RUNTIME=development
	@m4 $(M4_OPTS) dobi.yaml.m4 > dobi.yaml

docker-compose.dev.yaml: docker-compose.dev.m4.yaml Makefile
	@echo "\n + + + Build docker-compose.dev.yaml + + + "
	@m4 $(M4_OPTS) --define m4Runtime=development  docker-compose.dev.m4.yaml > docker-compose.dev.yaml

$(dobiTargets): $(dobiDeps)
	$(if $(VERSION_TAG),,$(error: set project version string on VERSION_TAG, when calling this task))
	@echo "\n + + + Do it with version $(VERSION_TAG) + + + "
	@dobi $@

clean: | autoclean
	@rm -rf .dobi dobi.yaml Dockerfile kubernetes/app.production.yaml kubernetes/app.development.yaml docker-compose.dev.yaml

release:
	$(if $(VERSION_TAG),,$(error: set project version string on VERSION_TAG, when calling this task))
	@echo "\n + + + Set next version: $(VERSION_TAG) + + + "
	@make clean
	@make docker-compose.dev.yaml
	@make kubernetes/app.production.yaml
	@make kubernetes/app.development.yaml
	@make docker-compose.dev.yaml
	@echo "\n + + + Push tags to repository + + + "
	@git add .
	@git commit -m "Changes for next release $(VERSION_TAG)"
	@git tag -s $(VERSION_TAG) -m "Next release $(VERSION_TAG)"
	@git push --tags origin master


docker_login:
	@docker login -p$(REGISTRY_PASSWORD) -u$(REGISTRY_USER) $(REGISTRY)

docker_logout:
	@docker logout $(REGISTRY)
